import React from "react";
import { Table,  } from "react-bootstrap";
import { WikiContent } from "../structures";

class WikiComponent extends React.Component<WikiContent> {
  render() {
	return (
	  <div className="wikiBody">
		<h1>
		  <span>{this.props.label}</span>
		</h1>
		<div className="wikiMainContent">
			<div>
				<h2 className="wikiContentTitle">Wikipedia Comment</h2>
				<span>{this.props.comment}</span>
			</div>
		</div>
		<div className="wikiSideBar">
			<img src={this.props.thumbnail} alt={this.props.label} />
			<Table bordered>
				<thead>
					<tr>
						<th>
							Property
						</th>
						<th>
							Value
						</th>
					</tr>
				</thead>
				<tbody>
					{!!this.props.areaMetro ?
						<tr>
							<td>Metropolitan Area</td>
							<td>{this.props.areaMetro || "NaN"} km²</td>
						</tr>: <></>
					}
					{!!this.props.areaTotal ?
						<tr>
							<td>Area Total</td>
							<td>{this.props.areaTotal || "NaN"} km²</td>
						</tr>: <></>
					}
					{!!this.props.populationDensity ?
						<tr>
							<td>Population Density</td>
							<td>{this.props.populationDensity || "NaN"} / km²</td>
						</tr> : <></>
					}
					{!!this.props.populationTotal ?
						<tr>
							<td>Population Total</td>
							<td>{this.props.populationTotal || "NaN"}</td>
						</tr>: <></>
					}
				</tbody>
			</Table>
		</div>
	  </div>
	);
  }
}

export default WikiComponent;
