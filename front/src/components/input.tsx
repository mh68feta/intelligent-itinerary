import React from "react";
import { InputGroup, Button, Form } from "react-bootstrap";
import { AsyncTypeahead } from "react-bootstrap-typeahead";

import App from "./app";
import { Suggestion } from "../structures";
import { pppSuggestConnector } from "../connectors/apiConnector";

interface InputProps {
	appRef: App;
	flip: boolean;
}

class Input extends React.Component<InputProps> {
	state: {
		value: string,
		options: Suggestion[],
		selected: Suggestion[],
		isLoading: boolean,
		valid: boolean
	} = {
		value: "",
		options: [],
		selected: [],
		isLoading: false,
		valid: false
	}

	connector = new pppSuggestConnector();

	handleSearch(value: string) {
		this.setState({
			isLoading: true,
			valid: false
		});
		this.connector.fetchContent({ label: value })
			.then(options => {
				this.setState({
					options: this.props.flip? options.reverse(): options,
					value: value,
					isLoading: false
				});
		});
	}

	handleChange(selected: Suggestion[]) {
		this.setState({
			selected: selected,
			valid: true
		});
	}

	render() {
		return (
			<Form 
				validated={this.state.valid}
				onSubmit={(e: any) => {
					e.preventDefault();
					const selected = this.state.selected;
					if (this.state.valid) {
						this.setState({
							selected: [],
							value: "",
							valid: false
						});
						this.props.appRef.addPlace(selected);
					}
			}}>
				<InputGroup 
					className="mb-3"
					style={{width: "100%", margin: "auto", marginTop: 5, marginLeft: 5}}
				>
					<AsyncTypeahead
						isLoading={this.state.isLoading}
						onSearch={value => {this.handleSearch(value)}}
						onChange={selected => {this.handleChange(selected)}}
						options={this.state.options}
						placeholder="Type to find a place..."
						id="#poiSuggest"
						dropup={this.props.flip}
						aria-label="Populated Place"
						aria-describedby="basic-addon2"
						labelKey={option => option.label}
						selected={this.state.selected}
						autoFocus
						maxHeight={"750px"}
						highlightOnlyResult
					/>
					<InputGroup.Append>
						<Button className="sideBarButton" variant="outline-secondary" type="submit" >
								Add
						</Button>
					</InputGroup.Append>
				</InputGroup>
			</Form>
		);
	}
}

export default Input;
