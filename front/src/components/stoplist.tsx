import React from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Button, ListGroup, Popover, Badge, OverlayTrigger } from "react-bootstrap";
import { Adjust } from "@material-ui/icons";

import { PlaceData, POIData } from "../structures";
import App from "./app";

const reorder = (list: PlaceData[], startIndex: number, endIndex: number) => {
	const result = Array.from(list);
	const [removed] = result.splice(startIndex, 1);
	result.splice(endIndex, 0, removed);

	return result;
};

interface StopListProps {
	appRef: App;
}

class StopList extends React.Component<StopListProps> {

	constructor(props: StopListProps) {
		super(props);
		this.onDragEnd = this.onDragEnd.bind(this);
	}
	
	onDragEnd(result: any) {
		// dropped outside the list
		if (!result.destination) {
			return;
		}
	
		const items = reorder(
			this.props.appRef.state.places,
			result.source.index,
			result.destination.index
		);
	
		this.props.appRef.setState({
			...this.props.appRef.state,
			marked: result.destination.index,
			loading: true,
			places: items
		})
	}

	render() {
		const places = this.props.appRef.state.places;
		return (
			<DragDropContext onDragEnd={this.onDragEnd}>
				<Droppable 
					droppableId="droppable"
				>
					{(provided, snapshot) => (
					<div
						{...provided.droppableProps}
						ref={provided.innerRef}
						className={"droppable"}
					>
						{places.map((item, index) => (
							<Draggable key={item.label} draggableId={item.label} index={index}>
								{(provided, snapshot) => (
									<div
										ref={provided.innerRef}
										{...provided.draggableProps}
										{...provided.dragHandleProps}
									>
										<SelectedItem
											id={item.label}
											content={item.pois}
											position={
												places.length === 1 ? "sole" :
												index === 0 ? "start" : 
												index === places.length - 1 ? "end" : "middle"
											}
											index={index}
											appRef={this.props.appRef}
										/>
									</div>
								)}
							</Draggable>
						))}
						{provided.placeholder}
					</div>
				)}
				</Droppable>
			</DragDropContext>
		);
	}
}

interface SelectedItemProps {
	id: string;
	content: POIData[];
	position: "start" | "middle" | "end" | "sole";
	index: number;
	appRef: App;
}

class SelectedItem extends React.Component<SelectedItemProps> {
	render() {
		const popover = (
			<Popover id={`popover_${this.props.index}`} className="poiPopover">
				<Popover.Title as="h3">Selected POIs</Popover.Title>
				<Popover.Content>
					<ListGroup className="poiList">
						{this.props.content.map((poi, index) => this.poiItem(poi, index))}
					</ListGroup>
				</Popover.Content>
			</Popover>
		);
		return (
			<div className="draggable" style={this.props.index === this.props.appRef.state.marked ? {backgroundColor: "rgba(0,255,0,0.1)"}: {}}>
				<Button variant="danger" onClick={() => {
					this.props.appRef.removeItem(this.props.index);
				}}>{"Remove"}</Button>
				<div 
					style={{
						height: "100%"
					}}
					onClick={() => {
						this.props.appRef.setState({
							...this.props.appRef.state,
							loading: true,
							marked: this.props.index
						});
					}}
				>
					<p>{this.props.id}</p>
					<svg>
						<line 
							x1="25"
							y1={
								this.props.position === "sole" ? "25" :
								this.props.position === "start" ? "25" : "0"
							}
							x2="25"
							y2={
								this.props.position === "sole" ? "25" :
								this.props.position === "end" ? "25" : "50"
							}
							style={
								{
									stroke: "midnightblue",
									strokeWidth: "5"
								}
							}/>
						<circle cx="25" cy="25" r="20" fill="darkorange"/>
					</svg>
					<OverlayTrigger trigger="click" placement="right" overlay={popover}>
						<Badge className="poiBadge" variant={this.props.content.length > 0 ? "success" : "secondary"}>
							{this.props.content.length}
						</Badge>
					</OverlayTrigger>
				</div>
			</div>
		);
	}

	poiItem(poiData: POIData, index: number) {
		return (
			<ListGroup.Item className="poiListItem" key={`poi_${index}`}>
				<span>{poiData.name}</span>
				<Adjust className="poiListIcon" style={{color: "green"}}/>
			</ListGroup.Item>
		);
	} 
}

export default StopList;
