import React from "react";
import { ScaleLoader } from "react-spinners";

class Loading extends React.Component {
	render() {
		return (
			<div className={"loading"}>
				<div className={"box"}>
					<ScaleLoader color={"darkorange"} loading={true} height={35} width={10} css={"margin-top: 17.5px"}/>
				</div>
			</div>
		);
	}
}

export default Loading;
