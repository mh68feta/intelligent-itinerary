import React from "react";
import ReactDOMServer from "react-dom/server";
import { Map, TileLayer, Popup, Marker} from "react-leaflet";
import { LocationCity, AccountBalance } from "@material-ui/icons";
import { Badge } from "react-bootstrap";
import L from "leaflet";

import { PlaceData, POIData } from "../structures";
import PopupContent from "./popup";
import App from "./app";

interface Props {
	lat: number;
	lng: number;
	zoom: number;
	height?: string;
	width?: string;
	placeData: PlaceData[];
	marked: number;
	poiData: POIData[];
	appRef: App;
}

const MapComponent: React.FC<Props> = ({lat, lng, zoom, height, width, placeData, marked, poiData, appRef}) => {
	const markedPlaceData = placeData[marked];
	const markedPlaceMarkerIcon = L.divIcon({
		className: "markerIcon",
		iconSize: [35, 35],
		html: ReactDOMServer.renderToString(
			<Badge variant="light" className="markerBadge">
				<LocationCity className="markedPlaceMarker"/>
			</Badge>
		)
	});
	const poiMarkerIcon = L.divIcon({
		className: "markerIcon",
		iconSize: [35, 35],
		html: ReactDOMServer.renderToString(
			<Badge variant="light" className="markerBadge">
				<AccountBalance className="POIMarker"/>
			</Badge>
		)
	});
	const markedPoiMarkerIcon = L.divIcon({
		className: "markerIcon",
		iconSize: [35, 35],
		html: ReactDOMServer.renderToString(
			<Badge variant="light" className="markerBadge">
				<AccountBalance className="markedPOIMarker"/>
			</Badge>
		)
	});

	const placeMarker = placeData.map((place, index) => {
		return (
			<Marker
				key={`markedPlaceMarker_${index}`}
				icon={markedPlaceMarkerIcon}
				position={[place.pos.lat, place.pos.lng]}
			>
				<Popup className="popup">
					<PopupContent
						label={place.label}
						comment={place.comment}
						pictures={place.pictures}
						type="city"
						index={index}
						appRef={appRef}
						pos={place.pos}
					/>
				</Popup>
			</Marker>
		);
	});

	const markedPlacePOIMarkers = markedPlaceData.pois.map((poi: POIData, index) => {
		return (
			<Marker 
				key={`markedPoiMarker_${index}`}
				icon={markedPoiMarkerIcon}
				position={[poi.pos.lat, poi.pos.lng]}
			>
				<Popup className="popup">
					<PopupContent
						label={poi.name}
						comment={poi.comment}
						pictures={poi.pictures}
						type="poi"
						index={index}
						appRef={appRef}
						pos={poi.pos}
					/>
				</Popup>
			</Marker>
		);
	});

	const poiMarkers = poiData.map((poi: POIData, index: number) => {
		return (
			<Marker
				key={`poiMarker_${index}`}
				icon={poiMarkerIcon}
				position={[poi.pos.lat, poi.pos.lng]}
			>
				<Popup className="popup">
					<PopupContent
						label={poi.name}
						comment={poi.comment}
						pictures={poi.pictures}
						type="unmarkedPoi"
						index={index}
						appRef={appRef}
						pos={poi.pos}
					/>
				</Popup>
			</Marker>
		);
	});

	return (
		<Map center={[lat, lng]} zoom={zoom} style={{
				height: height || 720, 
				maxWidth: width || 920,
				zIndex: 1
			}}>
			<TileLayer
    	      attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    	      url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    	    />
			{markedPlacePOIMarkers }
			{poiMarkers}
			{placeMarker}
		</Map>
	);
}

MapComponent.defaultProps = {
	lat: 51.505,
	lng: -0.09,
	zoom: 10,
	height: "720",
	width: "920"
}

export default MapComponent;