import React from "react";
import { Image } from "react-bootstrap";
import Input from "./input";
import App from "./app";

class StartupPage extends React.Component<{ appRef: App }> {
	render() {
		return (
			<div className="startupContent" style={{position: "fixed", bottom: "0", height: "100%", width: "100%"}}>
				<Image src={"https://images.unsplash.com/photo-1572690709015-d1f57d64a1d4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80"}
					style={{
						backgroundSize: "100%",
						width: "100%",
						height: "100%",
						pointerEvents: "none",
						userSelect: "none",
						MozUserSelect: "none",
						WebkitUserSelect: "none"
					}}
					unselectable="on"
					alt={"Photo by Markus Winkler on Unsplash"}
				></Image>
				<div className="searchContainer" style={{position: "fixed", bottom: "45%", left: "35%", width: "30%"}}>
					<Input appRef={this.props.appRef} flip={false}/>
				</div>
				<div style={{position: "fixed", bottom: 0, right: 0, padding: 5, fontFamily: "sans-serif", backgroundColor: "white", height: "35px", opacity: 0.6}}>
					<p>Photo by <a href="https://unsplash.com/@markuswinkler?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Markus Winkler</a></p>
				</div>
			</div>
		);
	}
}

export default StartupPage;