import React from "react";

import WikiComponent from "./wikiComponent";
import WeatherComponent from "./weather";
import MapComponent from "./map";
import { Tab, AppState } from "./app";
import App from "./app";
import Loading from "./loading";
import TrainComponent from "./train";
import AboutComponent from "./about";

interface ContentProps {
	appRef: App,
	appState: AppState
}

class Content extends React.Component<ContentProps> {

	static defaultProps = {
		lat: 51.505,
		lng: -0.09,
		zoom: 10,
		height: "720",
		width: "920"
	}

	render() {
		const places = this.props.appState.places;
		const marked = this.props.appState.marked;
		const markedPlace = places[marked];
		const weatherData = this.props.appState.weatherData;

		if (!this.props.appState.loading) {
			switch (this.props.appState.tab) {
				case Tab.MAP:
					if (!!markedPlace) {
						return <MapComponent
							lat = {markedPlace.pos.lat}
							lng = {markedPlace.pos.lng}
							zoom = {Content.defaultProps.zoom}
							height = {"900px"}
							width = {"1920px"}
							placeData = {places}
							marked = {marked}
							poiData = {this.props.appRef.state.poiData}
							appRef={this.props.appRef}
						/>;
					} else {
						return <Loading/>;
					}
				case Tab.WIKI:
					if (!!this.props.appState.wikiData) {
						return <WikiComponent {...this.props.appState.wikiData}/>;
					} else { 
						return <Loading/>;
					}
				case Tab.WEATHER:
					if (!!weatherData) {
						return <WeatherComponent
							data = {weatherData}
						/>;
					} else {
						return <Loading/>;
					}
				case Tab.CONNECTION:
					if (places.length >= 2) {
						return <TrainComponent appState={this.props.appState} appRef={this.props.appRef}/>;
					} else {
						return <Loading/>;
					}
				case Tab.ABOUT:
					return <AboutComponent/>
				default:
					return <div className={"error"}>404</div>;
			}
		} else {
			return <Loading/>;
		}
	}
}

export default Content;