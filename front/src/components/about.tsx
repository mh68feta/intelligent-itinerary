import React from "react";

class AboutComponent extends React.Component {
	render() {
		return (
			<div className="aboutContainer">
				<h1>About</h1>
				<p>
					This application was created as part of a university project connected to the lecture <em>Wissens- und Contentmanagement</em> held by <b>Dr. Thomas Efer</b> at the University Leipzig.
					The goal of this project was to build an application that reads and stores different kinds of information from different sources and presents it as useful aggregated content objects to the user.
					Specifically we wanted to build an application that aggregates data from dbpedia, the Dark Sky API, train connection data provided by the Deutsche Bahn and potentially other sources to build a 
					smart travel guide for the user. The sourcecode for this application is available in the <a href="https://git.informatik.uni-leipzig.de/users/sign_in">GitLab</a> of the University Leipzig.
				</p>
				<h1>Licensing</h1>
				<ul>
					<li>Map Images and Cartography provided by © <a href="https://www.openstreetmap.org/copyright">OpenStretMap</a> contributors</li>
					<li>Background and Weather Images provided by <a href="https://unsplash.com/license">Unsplash</a></li>
					<li>Weather data provided by the <a href="https://darksky.net/dev">Dark Sky API</a></li>
					<li>Train connection data provided by the <a href="https://data.deutschebahn.com/">Deutsche Bahn</a></li>
					<li>
						Texts, Geodata and images provided by <a href="https://en.wikipedia.org/wiki/Wikipedia:Copyrights">Wikipedia</a>
						via <a href="https://wiki.dbpedia.org/">DBpedia</a> under the <a href="https://creativecommons.org/licenses/by-sa/3.0/de/">CC BY-SA 3.0</a> and
						<a href="http://www.gnu.org/licenses/fdl-1.3.html"> GFDL</a> Licenses
					</li>
				</ul>
			</div>
		);
	}
}

export default AboutComponent;
