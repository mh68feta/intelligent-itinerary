import React from "react";
import { Button } from "react-bootstrap";

interface CookieFooterProps {
	enabled: number;
	cookieFunction(val: boolean): void;
}

class CookieFooter extends React.Component<CookieFooterProps> {
	render() {
		return (
			<div className="footer" style={
				this.props.enabled === -1 ? 
				{opacity: "100%", height: "105px"} : 
				{opacity: "0%", height: "0px", pointerEvents: "none"}}>
				<span>
					This site is able to use cookies to store the user session for 24 hours.
					If you want to enable this feature, please press <em>Enable Cookies</em> on the right.
					The site may override your current selection once you make this decision.
				</span>
				<Button style={{bottom: "53px"}} variant="outline-primary" onClick={() => this.props.cookieFunction(true)}>Enable Cookies</Button>
				<Button style={{bottom: "3px"}} variant="outline-primary" onClick={() => this.props.cookieFunction(false)}>Disable Cookies</Button>
			</div>
		);
	}
}

export default CookieFooter;