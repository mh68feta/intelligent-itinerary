import React from "react";
import { Badge, Image, Button } from "react-bootstrap";
import { LocationCity, AccountBalance } from "@material-ui/icons";
import App from "./app";
import { Position } from "../structures";

interface PopupProps {
	label: string;
	type: "city" | "poi" | "unmarkedPoi";
	comment: string;
	pictures: string[];
	appRef: App;
	index: number;
	pos: Position;
}

class PopupContent extends React.Component<PopupProps> {
	buttons() {
		if(this.props.type === "unmarkedPoi") {
			return (
				<Button className="button" variant="success" style={{float: "right"}} onClick={() => {
					this.props.appRef.addPOI({
						name: this.props.label,
						comment: this.props.comment,
						pictures: this.props.pictures,
						pos: this.props.pos
					});
				}}>Add</Button>
			);
		}
		return (
			<Button className="button" variant="danger" style={{float: "right"}} onClick={() => {
				if (this.props.type === "city") {
					this.props.appRef.removeItem(this.props.index);
				} else {
					this.props.appRef.removePOI(this.props.index);
				}
			}}>Remove</Button>
		);
	}

	render() {
		const pictures = this.props.pictures || [];
		return (
			<div>
				<h3 className="popupHeader">
					<Badge className="popupBadge" variant="light">
						{this.props.type === "city" ? <LocationCity/> : <AccountBalance/>}
					</Badge>
					<span className="title">{this.props.label}</span>
				</h3>
				<div className="popupContent">
					{!!pictures[0] ? <div className="popupThumbnail"><Image className="image" src={pictures[0]}/></div> : <div/>}
					<p>{this.props.comment}</p>
				</div>
				<div className="popupFooter">
					{this.buttons()}
				</div>
			</div>
		);
	}
}

export default PopupContent;
