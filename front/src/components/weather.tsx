import React from "react";
import { WiRaindrop, WiStrongWind, WiThermometer, WiThermometerExterior } from "weather-icons-react";
import { Card, CardGroup } from "react-bootstrap";

import Loading from "./loading";
import { WeatherData, WeatherDayProps } from "../structures";


interface WeatherProps {
	data: WeatherData;
};

class WeatherComponent extends React.Component<WeatherProps> {
	state = {
		data: this.props.data
	}

	render() {
		const days = this.props.data.days;
		return (
			<div>
				<CardGroup>
					<WeatherCard key={`weather_panel_${0}`} data={days[0]}/>
					<WeatherCard key={`weather_panel_${1}`} data={days[1]}/>
					<WeatherCard key={`weather_panel_${2}`} data={days[2]}/>
				</CardGroup>
				<CardGroup>
					<WeatherCard key={`weather_panel_${3}`} data={days[3]}/>
					<WeatherCard key={`weather_panel_${4}`} data={days[4]}/>
					<WeatherCard key={`weather_panel_${5}`} data={days[5]}/>
					<WeatherCard key={`weather_panel_${6}`} data={days[6]}/>
				</CardGroup>
			</div>
		);
	}
}

class WeatherCard extends React.Component<{data: WeatherDayProps}> {
	render() {
		if (this.props.data) {
			const data = this.props.data;
			return (
				<Card className={"weatherPanel"}>
					<Card.Img className={"weatherImage"} variant="top" src={this.getWeatherImage(data.icon)}/>
					<Card.ImgOverlay>
						<h1>{data.icon.toUpperCase().split("-").join(" ")}</h1>
					</Card.ImgOverlay>
					<Card.Header><b>{data.date}</b></Card.Header>
					<Card.Body>
						<p>
							<b className={"red"}>
								<WiThermometer/>
								{` ${Math.floor(data.temperatureHigh * 100) / 100}°C `}
							</b>
							<b className={"blue"}>
								/
								<WiThermometerExterior/>
								{` ${Math.floor(data.temperatureLow * 100) / 100}°C`}
							</b>
						</p>
						<p>
							<b className={"blue"}>
								<WiRaindrop/>
								{` ${Math.floor(data.precipIntensity * 1000 * 100) / 100}ml / ${Math.floor(data.precipProbability * 100 * 100) / 100}%`}
							</b>
						</p>
						<p>
							<b className={"blue"}>
								<WiStrongWind/>
								{` ${Math.floor(data.windSpeed * 100) / 100} km/h`}
							</b>
						</p>
					</Card.Body>
				</Card>
			);
		}
		else {
			return <Loading/>;
		}
	}

	getWeatherImage(type: string): string {
		switch(type) {
			case "sleet": {
				return "https://images.unsplash.com/photo-1444384851176-6e23071c6127?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80";
			}
			case "snow": {
				return "https://images.unsplash.com/photo-1514632595-4944383f2737?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80";
			}
			case "rain": {
				return "https://images.unsplash.com/photo-1486016006115-74a41448aea2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80";
			}
			case "storm": {
				return "https://images.unsplash.com/photo-1508697014387-db70aad34f4d?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80";
			}
			case "cloudy": {
				return "https://images.unsplash.com/photo-1534088568595-a066f410bcda?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80";
			}
			case "hail": {
				return "https://images.unsplash.com/photo-1566205190430-536f5c93d8ca?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80";
			}
			case "clear-day": {
				return "https://images.unsplash.com/photo-1540308990836-5a7b1df6dc00?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80"
			}
			case "partly-cloudy-day": {
				return "https://images.unsplash.com/photo-1578993286884-b4fb33a6b306?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80";				
			}
			default: {
				console.log(`found type: ${type}`);
				return "https://images.unsplash.com/photo-1578993286884-b4fb33a6b306?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=80";
			}
		}
	}
}

export default WeatherComponent;

