import React from "react";
import App from "./app";
import { InputGroup, Button, FormControl, Form } from "react-bootstrap";
import { AsyncTypeahead } from "react-bootstrap-typeahead";
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { Suggestion } from "../structures";
import { POISuggestConnector } from "../connectors/apiConnector";

interface POISearchProps {
	appRef: App;
}

class POISearchBar extends React.Component<POISearchProps> {
	state: {
		category: string,
		range: string,
		validRange: boolean,
		validCategory: boolean,
		isLoading: boolean,
		options: Suggestion[],
		selected: Suggestion[]
	} = {
		category: "",
		range: "100",
		isLoading: false,
		validRange: true,
		validCategory: false,
		selected: [],
		options: []
	}

	connector = new POISuggestConnector();

	handleRangeChange(event: any) {
		if (!Number(event.target.value)) {
			this.setState({
				validRange: false,
				range: event.target.value
			});
		} else {
			this.setState({
				validRange: true,
				range: event.target.value
			});
		}
	}

	handleChange(selected: Suggestion[]) {
		this.setState({
			validCategory: true,
			selected: selected
		});
	}

	handleSearch(value: string) {
		this.setState({
			isLoading: true,
			validCategory: false
		});
		this.connector.fetchContent({ label: value })
			.then(options => {
				this.setState({
					options: options.reverse(),
					category: value,
					isLoading: false
				});
		});
	}

	render() {
		return (
			<Form 
				validated={this.state.validRange && this.state.validCategory}
				onSubmit={(e: any) => {
					e.preventDefault();
					if (this.state.selected.length > 0) {
						if (this.state.category.length > 0 && this.state.validRange && !!this.state.selected) {
							this.setState({
								category: "",
								selected: [],
								validCategory: false
							})
							this.props.appRef.addPOIData(this.state.selected[0], Number(this.state.range));
						}
					}
				}}
			>
				<InputGroup 
					className="mb-3"
					style={{width: "100%", marginTop: 5, marginLeft: 5}}
				>
					<AsyncTypeahead
						isLoading={this.state.isLoading}
						onSearch={value => {this.handleSearch(value)}}
						onChange={selected => {this.handleChange(selected)}}
						options={this.state.options}
						placeholder="POI Category"
						id="#poiSuggest"
						dropup
						aria-label="PoI-Category"
						aria-describedby="basic-addon2"
						labelKey={option => option.label}
						selected={this.state.selected}
						maxHeight={"750px"}
						highlightOnlyResult

					/>
					<InputGroup.Append>
						<FormControl
							className="rangeInput"
							value={this.state.range}
							placeholder="Range"
							aria-label="Range"
							aria-describedby="basic-addon2"
							onChange={this.handleRangeChange.bind(this)}
							style={{
								fontFamily: "sans-serif",
								borderRadius: 0
							}}
							autoFocus
						/>
						<Button className="sideBarButton" variant="outline-secondary" type="submit" >
								Show
						</Button>
					</InputGroup.Append>
				</InputGroup>
			</Form>
		);
	}
}

export default POISearchBar;
