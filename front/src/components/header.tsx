import React from "react";
import { Navbar} from "react-bootstrap";

interface Props {
  siteTitle: string
}

class Header extends React.Component<Props> {
  render() {
	return (
		<header
			style={{
				marginBottom: `1.45rem`,
				zIndex: 3
			}}
		>
			<Navbar bg="light" variant="light" className="header">
				<Navbar.Brand>
					<img className="logo" src="WCMLogo.svg" alt="Travel Guide" width="100" height="50"/>
					{this.props.siteTitle}
				</Navbar.Brand>
				{this.props.children}
			</Navbar>
		</header>
	);
  }
}

export default Header
