import React from "react";
import { Fab } from "@material-ui/core";
import { Map as MapIcon, Cloud as CloudIcon, Info as InfoIcon, Train as TrainIcon} from "@material-ui/icons";

import Header from "./header";
import Content from "./content";
import Input from "./input";
import { PlaceData, WeatherData, POIData, TrainJourney, Suggestion, WikiContent } from "../structures";

import { DataStore } from "../connectors/dataStore";
import StartupPage from "./startup";
import { TrainConnector } from "../connectors/apiConnector";
import POISearchBar from "./poiSearchBar";
import StopList from "./stoplist";
import CookieFooter from "./cookieFooter";
import { Nav } from "react-bootstrap";

export enum Tab {
	MAP = 0,
	WIKI = 1,
	WEATHER = 2,
	CONNECTION = 3,
	ABOUT = 4
}

export interface AppState {
	places: PlaceData[];
	weatherData: WeatherData | undefined;
	wikiData: WikiContent | undefined;
	placeQueue: string[];
	poiQueue: {
		category: Suggestion;
		range: number;
	}[];
	poiData: POIData[];
	marked: number;
	tab: Tab;
	poiCategory: string,
	poiRange: number;
	loading: boolean;
	cookiesEnabled: number;
}

interface AppProps {
	title: string;
}

class App extends React.Component<AppProps> {
	state: AppState;
	store: DataStore;
	empty: boolean = true;

	trainConnector = new TrainConnector();

	constructor(props: AppProps) {
		super(props);
		this.store = new DataStore();
		this.state = {
			marked: 0,
			tab: Tab.MAP,
			weatherData: undefined,
			wikiData: undefined,
			placeQueue: [],
			poiQueue: [],
			poiData: [],
			places: [],
			poiCategory: "",
			poiRange: 10,
			loading: false,
			cookiesEnabled: this.store.getCookieToggle()
		};
		
	}

	addPlace(value: Suggestion[]) {
		const queue = this.state.placeQueue;
		queue.push(value[0].label);
		this.setState({
			...this.state,
			loading: true,
			queue: queue
		});
	}

	isLoading() {
		return this.state.poiQueue.length > 0 || this.state.poiQueue.length > 0;
	}

	componentDidMount() {
		if (this.state.cookiesEnabled === 1) {
			this.store.loadCookies().then(newPlaces => {
				this.setState({
					...this.state,
					places: newPlaces
				});
			});
		}
	}

	componentDidUpdate() {
		const placeQueue = this.state.placeQueue;
		const places = this.state.places;
		const marked = this.state.marked;
		if (this.state.loading) {
			if (placeQueue.length > 0) {
				this.store.getPlaceData(placeQueue.pop()!).then(newPlaceData => {
					places.push(newPlaceData);
					this.setState({
						...this.state,
						queue: placeQueue,
						loading: this.isLoading(),
						placeData: places,
						marked: places.length - 1
						});
				});
			} else if (this.state.poiQueue.length > 0) {
				const query = this.state.poiQueue.pop()!;
				this.store.getPOIData(places[marked].label, places[marked].pos, query.range, query.category.iri)
					.then(newPOIData => {
						console.log(newPOIData);
						this.setState({
							...this.state,
							loading: this.isLoading(),
							poiData: newPOIData
						})
					});
			} else if (this.state.tab === Tab.WIKI) {
				this.store.getWikiData(places[marked].iri)
					.then(newWikiContent => {
						this.setState({
							...this.state,
							loading: false,
							wikiData: newWikiContent
						});
					});
			} else if(this.state.tab === Tab.WEATHER) {
				this.store.getWeatherData(places[marked].label, places[marked].pos)
					.then(newWeatherData => {
						this.setState({
							...this.state,
							loading: false,
							weatherData: newWeatherData
						});
					});
			} else {
				this.setState({
					...this.state,
					loading: false
				});
			}
		} else {
			if (this.state.cookiesEnabled === 1) {
				this.store.saveCookies(this.state);
			}
		}
	}

	addPOIData(poiCategory: Suggestion, poiRange: number) {
		const queue = this.state.poiQueue;
		queue.push({
			category: poiCategory,
			range: poiRange
		});
		this.setState({
			...this.state,
			loading: true,
			poiQueue: queue
		});
	}

	removeItem(index: number) {
		const result = Array.from(this.state.places);
		result.splice(index, 1);
		this.setState({
			...this.state,
			loading: true,
			marked: 0,
			places: result
		})
	};

	addPOI(poi: POIData) {
		const places = this.state.places;
		const place = places[this.state.marked];
		const pois = place.pois;
		pois.push(poi);
		const newPlace = {
			...place,
			pois: pois
		};
		places.splice(this.state.marked, 1, newPlace);
		this.setState({
			...this.state,
			loading: true,
			places: places 
		});
	}

	removePOI(index: number) {
		const places = this.state.places;
		const place = places[this.state.marked];
		const pois = place.pois;
		pois.splice(index, 1);
		const newPlace = {
			...place,
			pois: pois
		};
		places.splice(this.state.marked, 1, newPlace);
		this.setState({
			...this.state,
			loading: true,
			places: places 
		});
	};

	fetchJourneys(startIndex: number, date: string): Promise<TrainJourney[]> {
		return new Promise((resolve, reject) => {
			this.trainConnector.fetchContent({
				from: this.state.places[startIndex].label,
				to: this.state.places[startIndex + 1].label,
				date
			}).then(journeys => {
				resolve(journeys)
			});
		});
	}

	switchTab(tab: Tab) {
		this.setState({
			...this.state,
			tab,
			loading: true
		});
	}

	buttons = () => (
		<div>
			<Fab className={"tabSwitch"} aria-label="map" style={{
					background: this.state.tab === Tab.MAP ? "midnightblue" : "darkorange",
					bottom: 350
				}}
				onClick={() => {
					this.switchTab(Tab.MAP);
				}}>
				<MapIcon />
			</Fab>
			<Fab className={"tabSwitch"} aria-label="wiki" style={{
					background: this.state.tab === Tab.WIKI ? "midnightblue" : "darkorange",
					bottom: 250
				}}
				onClick={() => {
					this.switchTab(Tab.WIKI);
				}}>
				<InfoIcon />
			</Fab>
			<Fab className={"tabSwitch"} aria-label="weather" style={{
					background: this.state.tab === Tab.WEATHER ? "midnightblue" : "darkorange",
					bottom: 150
				}}
				onClick={() => {
					this.switchTab(Tab.WEATHER);
				}}>
				<CloudIcon />
			</Fab>
			<Fab className={"tabSwitch"} aria-label="connection" style={{
					background: this.state.tab === Tab.CONNECTION ? "midnightblue" : "darkorange",
					bottom: 50
				}}
				onClick={() => {
					this.switchTab(Tab.CONNECTION);
				}}>
				<TrainIcon />
			</Fab>
		</div>
	);

	aboutLink = () => (
		<Nav.Item>
			<button onClick={() => {
				this.state.tab !== Tab.ABOUT ? this.switchTab(Tab.ABOUT) : this.switchTab(Tab.MAP);
			}}>{this.state.tab !== Tab.ABOUT ? "About": "Back to the App"}</button>
		</Nav.Item>
	);

	render() {
		if( this.state.places.length === 0 && this.state.tab !== Tab.ABOUT) {
			return (
				<div>
					<Header siteTitle={this.props.title}>
						{this.aboutLink()}
					</Header>
					<StartupPage appRef={this}/>
					<CookieFooter enabled={this.state.cookiesEnabled} cookieFunction={value => {
						this.store.setCookieToogle(value? 1: 0);
						this.setState({
							...this.state,
							cookiesEnabled: value
						});
					}}/>
				</div>
			);
		}
		return (
		<div>
			<Header siteTitle={this.props.title}>
				{this.aboutLink()}
			</Header>
			<div className={"sideBar"} style={{paddingLeft: 0, paddingRight: 10}}>
				<div className={"graph"} style={{height: "80%"}}>
					<StopList appRef={this}/>
				</div>
				<div className="searchContainer" style={{width: "100%", height: "20%"}}>
					<POISearchBar appRef={this}/>
					<Input appRef={this} flip/>
				</div>
			</div>
			<div className={"mainContent"}>
				{this.emptyContent()}
			</div>
			<CookieFooter enabled={this.state.cookiesEnabled} cookieFunction={value => {
				this.store.setCookieToogle(value? 1: 0);
				if (value) {
					this.store.loadCookies().then(newPlaces => {
						this.setState({
							...this.state,
							places: newPlaces,
							cookiesEnabled: value? 1: 0
						});
					});
				} else {
					this.setState({
						...this.state,
						cookiesEnabled: value? 1: 0
					});
				}
			}}/>
		</div>
		);
	}

	emptyContent() {
		if (this.state.places.length === 0 && this.state.tab !== Tab.ABOUT) {
			return (
				<div className="emptyContent">
					<p>SELECT A STARTING POINT</p>
				</div>
			);
		} else {
			return (
				<div>
					<Content
						appRef={this}
						appState={this.state}
					/>
					{this.state.tab !== Tab.ABOUT ? this.buttons(): <></>}
				</div>
			);
		}
	}
}

export default App;
