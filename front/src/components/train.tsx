import React from "react";
import { Card, Button, Form, CardGroup, Nav } from "react-bootstrap";
import DateTimePicker from "react-datetime-picker";
import { ArrowForward, Cached, FlightTakeoff, FlightLand } from "@material-ui/icons";

import App, { AppState } from "./app";
import { PlaceData, TrainJourney } from "../structures";

interface TrainCompProps {
	appState: AppState;
	appRef: App;
}

interface Sequence {
	start: PlaceData;
	end: PlaceData;
	startIndex: number;
}

class TrainComponent extends React.Component<TrainCompProps> {
	state: {
		sequences: Sequence[]
	}

	constructor(props: TrainCompProps) {
		super(props);
		const sequences: Sequence[] = [];
		props.appState.places.forEach((place, index) => {
			if (index < props.appState.places.length - 1) {
				sequences.push({
					start: place,
					end: props.appState.places[index + 1],
					startIndex: index
				});
			}
		});
		this.state = {
			sequences
		}
	}
	
	render() {
		return (
			<div>
				{this.state.sequences.map((seq, index) => {
					return <TrainCard key={`trainCard_${index}`} sequence={seq} appRef={this.props.appRef}/>;
				})}
			</div>
		);
	}
}

interface TrainCardProps {
	appRef: App;
	sequence: Sequence;
	journeys?: TrainJourney[];
}

interface TrainCardState {
	sequence: Sequence;
	tab: number;
	journeys?: TrainJourney[];
}

export class TrainCard extends React.Component<TrainCardProps> {
	state: TrainCardState;

	constructor(props: TrainCardProps) {
		super(props);
		this.state = {
			sequence: props.sequence,
			journeys: props.journeys,
			tab: 0
		};
	}

	render() {
		const journeys = !!this.state.journeys ? this.state.journeys.filter(j => j.price !== null): [];
		const journeyCards = journeys.map((journey, index) => JourneyCards(journey, index));
		return (
			<Card className="trainCard">
				<Card.Header className="trainCardHeader">
					<span className="journeyFormTitle">
						{this.props.sequence.start.label}
						<ArrowForward className="arrowIcon"/>
						{this.props.sequence.end.label}
					</span>
					<JourneyForm
						cardRef={this}
						appRef={this.props.appRef}
						index={this.props.sequence.startIndex}
					/>
					{journeys.length > 0 ?
						<Nav className="priceNav" variant="tabs" defaultActiveKey={0}>
							{
							journeys.map((journey, index) => {
								return (
									<Nav.Item key={`journeyPrice_${journey.price}_${index}`}>
										<Nav.Link className="journeyPrice" eventKey={index} onClick = {() => this.setState({...this.state, tab: index})}>
											{`${journey.price} €`}
										</Nav.Link>
									</Nav.Item>
								);
							})}
						</Nav> : <div style={{height: "0px"}}/>
					}
				</Card.Header>
				{
					journeys.length > 0 ?
						<Card.Body>
							{journeyCards[this.state.tab]}
						</Card.Body>
					: <div/>
				}
			</Card>
		);
	}

	renderJourney(journey: TrainJourney) {
		return <div>{JSON.stringify(journey, null, 4)}</div>;
	}
}

interface JourneyFormProps {
	cardRef: TrainCard;
	appRef: App;
	index: number;
}

class JourneyForm extends React.Component<JourneyFormProps> {
	state = {
		dateValue: new Date()
	}

	handleDateChange = (date: Date | Date[]) => this.setState({
		...this.state,
		dateValue: date
	});

	render() {
		return (
			<Form className="journeyForm" onSubmit={(e: any) => {
				e.preventDefault();
				const date = this.state.dateValue;
				const isoDate = date.toISOString().replace(/:\d\d\.\d\d\dZ/g, "");
				this.props.appRef.fetchJourneys(
					this.props.index,
					isoDate
				)
				.then(journeys => {
					this.props.cardRef.setState({
						...this.props.cardRef.state,
						journeys
					});
				});
			}}>
				<DateTimePicker
					className={"dateTimePicker"}
					onChange = {this.handleDateChange}
					value = {this.state.dateValue}
					format={"dd.MM.y | h:mm a"}
				/>
				<Button type="submit" className="journeyButton" variant="dark" style={{marginLeft: "10px", backgroundColor: "midnightblue"}}>
					<Cached/>
				</Button>
			</Form>
		);
	}
}

const JourneyCards: React.FC<TrainJourney> = (journey: TrainJourney, index: number) => {
	return (
		<CardGroup key={`journey_${index}`}>
			{journey.stops.map(stop => {
				const departTime = new Date(stop.departure.scheduled);
				const departA = departTime.getHours() > 12? "PM" : "AM"; 
				const departHours = departTime.getHours() > 12 ?
					departTime.getHours() - 12 : departTime.getHours();
				const departMinutes = departTime.getMinutes();

				const arrivalTime = new Date(stop.arrival.scheduled);
				const arrivalA = arrivalTime.getHours() > 12? "PM" : "AM"; 
				const arrivalHours = arrivalTime.getHours() > 12 ?
					arrivalTime.getHours() - 12 : arrivalTime.getHours();
				const arrivalMinutes = arrivalTime.getMinutes();
				
				const departure = (
					<Card className="departureCard">
						<Card.Header className="departureHeader">
							<FlightTakeoff className="departureIcon"/>
							{stop.departure.line}
							<ArrowForward className="arrowIcon"/>
							{stop.arrival.destination}
						</Card.Header>
						<Card.Body className="departureBody">
							<div className="time">
								{`${departHours}:${departMinutes} ${departA}`}
							</div>
							<div className="platform">
								Platform:
								<b className="platformNumber">
									{stop.departure.platform}
								</b>
							</div>
						</Card.Body>
					</Card>	
				);
				const arrival = (
					<Card className="arrivalCard">
						<Card.Header className="arrivalHeader">
							<FlightLand className="arrivalIcon"/>
							{stop.departure.line}
							<ArrowForward className="arrowIcon"/>
							{stop.arrival.destination}
						</Card.Header>
						<Card.Body className="arrivalBody">
							<div className="time">
								{`${arrivalHours}:${arrivalMinutes} ${arrivalA}`}
							</div>
							<div className="platform">
								Platform:
								<b className="platformNumber">
									{stop.arrival.platform}
								</b>
							</div>
						</Card.Body>
					</Card>
				);
				return {
					depart: departure,
					arrival: arrival
				}
			}).map((cards, index) => {
				return (
					<CardGroup className="journeySection" key={`section_${index}`}>
						{cards.depart}
						{cards.arrival}
					</CardGroup>
				);
			})}
		</CardGroup>
	);
}

export default TrainComponent;