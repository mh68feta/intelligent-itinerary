import React from "react";
import ReactDOM from 'react-dom';
import "./css/layout.css";
import "./css/index.css";
import App from "./components/app";

ReactDOM.render(
	<App title = {"Intelligent Itinerary - The DBpedia-Enriched Travel Guide"}/>,
	document.getElementById('root')
);