export interface PlaceData {
	iri: string;
	label: string;
	pos: Position;
	pois: POIData[];
	comment: string;
	pictures: string[];
};

export interface Position {
	lat: number;
	lng: number;
};

export interface POIData {
	name: string;
	pos: Position;
	comment: string;
	pictures: string[];
};

export interface WeatherData {
	days: WeatherDayProps[];
}

export interface WeatherDayProps {
	date: any,
	icon: string,
	precipIntensity: number,
	precipProbability: number,
	precipType: string,
	sunriseTime: any,
	sunsetTime: any,
	temperatureLow: number,
	temperatureHigh: number,
	temperatureHighTime: any,
	temperatureLowTime: any,
	timezone: string,
	windSpeed: number
}

export interface TrainStop {
	departure: {
		scheduled: string;
		platform: string;
		direction: string;
		line: string
	},
	arrival: {
		scheduled: string;
		platform: string;
		origin: string;
		destination: string;
	}
}

export interface TrainJourney {
	price: number;
	stops: TrainStop[];
}

export interface Sequence {
	start: PlaceData;
	end: PlaceData;
	journeys: TrainJourney[];
}

export interface Suggestion {
	iri: string;
	label: string;
}

export interface WikiContent {
	areaMetro: string,
	areaTotal: string,
	comment: string,
	iri: string,
	label: string,
	lat: string,
	long: string,
	populationDensity: string,
	populationTotal: string,
	thumbnail: string
}
