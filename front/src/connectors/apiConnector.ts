import Connector from "./connector";
import { PlaceData, POIData, WeatherData, Position, TrainJourney, Suggestion, WikiContent } from "../structures";

export class WeatherConnector implements Connector<WeatherData> {
	async fetchContent(data: {label: string, pos: Position}): Promise<WeatherData> {
		return new Promise((resolve, reject) => {
			fetch(`http://localhost:5000/weather/forecast/7days?lat=${data.pos.lat}&long=${data.pos.lng}`)
				.then(response => response.json())
				.then(responseData => {
					console.log(responseData);
					const weather: WeatherData = {
						days: responseData.day
					};
					resolve(weather);
				});
		});
	}
}

export class POIConnector implements Connector<POIData[]> {
	async fetchContent(data: {pos: Position, range: number, type: string}): Promise<POIData[]> {
		return new Promise((resolve, reject) => {
			const url = `http://localhost:5000/poi/range?dboType=${data.type}&lat=${data.pos.lat}&long=${data.pos.lng}&radius=${data.range}`
			fetch(url)
				.then(response => {
					return response.json()
				})
				.then(responseData => {
					const pois = responseData;
					const mappedPois = pois.map((p: any) => {
						return {
							comment: p.comment,
							name: p.label,
							pos: {
								lat: p.lat,
								lng: p.long
							},
							pictures: p.pictures
						}
					});
					resolve(mappedPois);
				})
		});
	}

	async fetchSinglePOI(data: {name: string}): Promise<POIData> {
		return new Promise((resolve, reject) => {
			const url = `http://localhost:5000/poi/info/${data.name}`;
			fetch(url)
				.then(response => {
					return response.json();
				})
				.then(responseData => {
					const poi: POIData = {
						name: responseData.label,
						pos: {
							lat: responseData.lat,
							lng: responseData.long
						},
						comment: responseData.comment,
						pictures: [responseData.thumbnail]
					};
					resolve(poi);
				});
		});
	}
}

export class WikiConnector implements Connector<WikiContent> {
	async fetchContent(data: {iri: string}): Promise<WikiContent> {
		return new Promise((resolve, reject) => {
			fetch(`http://localhost:5000/ppp/info?iri=${data.iri}`)
				.then(response => response.json())
				.then(responseData => {
					resolve(responseData);
				});
		});
	}
}

export class TrainConnector implements Connector<TrainJourney[]> {
	async fetchContent(data: {from: string, to: string, date: string}): Promise<TrainJourney[]> {
		return new Promise((resolve, reject) => {
			fetch(`http://localhost:5000/journey?from=${data.from}&to=${data.to}&date=${data.date}`)
				.then(response => response.json())
				.then(responseData => {
					resolve(responseData.journey);
				});
		});
	}
}

export class PlaceConnector implements Connector<PlaceData> {
	async fetchContent(data: {label: string}): Promise<PlaceData> {
		return new Promise((resolve, reject) => {
			fetch(`http://localhost:5000/ppp/info/${data.label}`)
				.then(response => {
					return response.json();
				}).then(responseData => {
					const poiData: POIData[] = [];
					const returnData: PlaceData = {
						iri: responseData.iri,
						label: responseData.label,
						pois: poiData,
						pos: {
							lat: responseData.lat,
							lng: responseData.long
						},
						comment: responseData.comment,
						pictures: [responseData.thumbnail]
					}
					resolve(returnData);
				});
		});
	}
}

export class POISuggestConnector implements Connector<Suggestion[]> {
	async fetchContent(data: {label: string}): Promise<Suggestion[]> {
		return new Promise((resolve, reject) => {
			fetch(`http://localhost:5000/poi/suggest/${data.label}`)
				.then(response => {
					return response.json();
				}).then(responseData => {
					resolve(responseData);
				});
		});
	}
}


export class pppSuggestConnector implements Connector<Suggestion[]> {
	async fetchContent(data: {label: string}): Promise<Suggestion[]> {
		return new Promise((resolve, reject) => {
			fetch(`http://localhost:5000/ppp/suggest/${data.label}`)
				.then(response => {
					return response.json();
				}).then(responseData => {
					resolve(responseData);
				});
		});
	}
}