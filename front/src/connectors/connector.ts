interface Connector<T> {
	fetchContent(data: any): Promise<T>;
}

export default Connector;
