import { PlaceConnector, WeatherConnector, POIConnector, WikiConnector } from "./apiConnector";
import { PlaceData, WeatherData, Position, POIData, WikiContent } from "../structures";
import * as Cookies from "js-cookie";
import { AppState } from "../components/app";

interface PoiQueryParameter {
	label: string,
	range: number,
	type: string
}

export class DataStore {
	placeStore = new Map<string, PlaceData>();
	weatherStore = new Map<string, WeatherData>();
	poiStore = new Map<PoiQueryParameter,POIData[]>(); 
	wikiStore = new Map<string, WikiContent>();

	placeConnector = new PlaceConnector();
	weatherConnector = new WeatherConnector();
	poiConnector = new POIConnector();
	wikiConnector = new WikiConnector();

	setCookieToogle(value: number) {
		Cookies.set("enabled", `${value}`);
	}

	getCookieToggle(): number {
		return parseInt(Cookies.get("enabled") || "-1");
	}

	saveCookies(appState: AppState) {
		Cookies.set(
			"selectedPlaces",
			JSON.stringify(
				appState.places.map(p => {
					Cookies.set(p.label, p.pois.map(poi => poi.name));
					return p.label;
				})
			)
		);
	}

	async loadCookies(): Promise<PlaceData[]> {
		return new Promise((resolve, reject) => {
			const selectedPlacesCookie = Cookies.get("selectedPlaces") || "[]";
			const selectedPlaces = JSON.parse(selectedPlacesCookie) as string[];
			resolve(Promise.all(
				selectedPlaces.map(placeLabel => {
					return this.getPlaceData(placeLabel)
						.then(placeData => {
							const pois = JSON.parse(Cookies.get(placeLabel) || "[]") as string[];
							return Promise.all(pois.map(poi => this.poiConnector.fetchSinglePOI({name: poi})))
								.then((poiData: POIData[]) => {
									return {
										iri: placeData.iri,
										label: placeData.label,
										comment: placeData.comment,
										pictures: placeData.pictures,
										pos: placeData.pos,
										pois: poiData
									}
								});
						});
			})));
		});
	}

	async getPlaceData(label: string): Promise<PlaceData> {
		return new Promise((resolve, reject) => {
			const data = this.placeStore.get(label);
			if (!!data) {
				resolve(data)
			} else {
				this.placeConnector.fetchContent({label})
					.then(place => {
						this.placeStore.set(label, place);
						resolve(place);
					});
			}
		});
	}

	async getWikiData(iri: string): Promise<WikiContent> {
		return new Promise((resolve, reject) => {
			const data = this.wikiStore.get(iri);
			if(!!data) {
				resolve(data);
			} else {
				this.wikiConnector.fetchContent({iri})
					.then(wikiContent => {
						this.wikiStore.set(iri, wikiContent);
						resolve(wikiContent);
					});
			}
		});
	}

	async getWeatherData(label: string, pos: Position): Promise<WeatherData> {
		return new Promise((resolve, reject) => {
			const data = this.weatherStore.get(label);
			if (!!data) {
				resolve(data)
			} else {
				this.weatherConnector.fetchContent({label, pos})
					.then(weather => {
						this.weatherStore.set(label, weather);
						resolve(weather);
					});
			}
		});
	}

	async getPOIData(label: string, pos: Position, range: number, type: string): Promise<POIData[]> {
		return new Promise((resolve, reject) => {
			const data = this.poiStore.get({
				label, range, type
			});
			if (!!data) {
				resolve(data)
			} else {
				this.poiConnector.fetchContent({pos, type, range})
					.then(poi => {
						this.poiStore.set({label, range, type}, poi);
						resolve(poi);
					});
			}
		});
	}

}
