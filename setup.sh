#!/usr/bin/env bash

# Intelligent Intrinary setup script

CLR_ERROR="\e[31;1m[ERROR]\e[m"
CLR_INFO="\e[32;1m[INFO]\e[m"
CLR_WARN="\e[33;1m[WARN]\e[m"

FLAG_BUILD="false"
[ "$1" = "--and-build" ] && FLAG_BUILD="true"

# check for dependecies
command -v docker > /dev/null || (echo -e "$CLR_ERROR docker command not found" && exit 1) 
command -v docker-compose > /dev/null || (echo -e "$CLR_ERROR docker-compose command not found" && exit 1)
command -v curl > /dev/null || (echo -e "$CLR_ERROR cUrl command not found" && exit 1)
git lfs &>/dev/null || (echo -e "$CLR_ERROR git or git-lfs not installed" && exit 1)

[ -d ".git" ] \
	&& echo -e "$CLR_INFO skipped clone repository" \
	|| ( echo -e "$CLR_INFO process clone repository" \
		&& git clone "https://git.informatik.uni-leipzig.de/mh68feta/intelligent-itinerary.git" \
		&& cd "intelligent-itinerary/" \
		&& git lfs pull)

# check for exisitng docker-compose build
# backend uses remote image
docker image inspect intelligent-itinerary_middleware > /dev/null || FLAG_BUILD="true"
docker image inspect intelligent-itinerary_frontend > /dev/null || FLAG_BUILD="true"

# build images
[ "$FLAG_BUILD" = "true" ] \
	&& echo -e "$CLR_INFO process building docker files" \
	&& (docker-compose build || (echo -e "$CLR_ERROR failed to docker-compose up -d" && exit 1))

# start containers
echo -e "$CLR_INFO process docker-compose up as deamons"
docker-compose up -d || (echo -e "$CLR_ERROR failed to docker-compose up -d" && exit 1)

# block and wait for startup
echo -e "$CLR_INFO process waiting for backend"
timeout 60 ./back/try-connect "localhost:9200" \
	&& echo -e " \e[32mdone\e[m" \
	|| (echo -e "$CLR_ERROR failed backend not reachable" && exit 1)

# load data
echo -e "$CLR_INFO process loading backend data"
back/load_suggest.sh "ppp_suggest" "back/dumps/PPP.json"
back/load_suggest.sh "poi_suggest" "back/dumps/POI.json"

echo -e "$CLR_INFO visit: http://localhost:8080 (API doc at port 5000)"
