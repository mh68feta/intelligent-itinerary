#!/usr/bin/env bash

# Script to filter the english DBpedia on bash to retrieve
# all dbo:City, dbo:Town, and dbo:Villages with their
# correspondig rdfs:labels.
# Further converting the results to json:
# { 
#   "label": "Leipzig",
#   "iri": "http://dbpedia.org/resource/Leipzig"
# }
# Used DBpedia version 2016-10 file list inside ./downloads

CLR_INFO="\e[32;1m[INFO]\e[m"
CLR_WARN="\e[33;1m[WARN]\e[m"
CLR_ERROR="\e[31;1m[ERROR]\e[m"

[ "$1" = "clean" ] \
	&& echo -e "$CLR_INFO process clean up" \
	&& rm -rf "rdf_dumps/" "ppp.nt.left" "labels.nt" "ppp_labels.nt" \
	&& exit 0

pre_ppp_iris()
{
	lbzcat $(find rdf_dumps -regex ".*instance_types.*") \
	| grep "City> .\|Town> .\|Village> ." \
	| cut -d" " -f1 \
	| LANG=C sort -u --parallel=8 \
	> $1 \
	|| (echo -e "$CLR_ERROR failed at pre_ppp_iris" && exit 1) 
}

pre_labels() {
	lbzcat rdf_dumps/labels_en.ttl.bz2 \
	| LANG=C sort -t' ' -k1 --parallel=8 \
	> $1 \
	|| (echo -e "$CLR_ERROR failed at pre_labels" && exit 1) 
}

join_labels_iris() {
	LANG=C join -1 1 -2 1 $1 $2 \
	> $3 \
	|| (echo -e "$CLR_ERROR failed at join_labels_iris" && exit 1) 
}

convert_ttl_json() {
	[ -f "./TurtleLabelsToJson.py" ] \
	&& ./TurtleLabelsToJson.py $1 \
	| LANG=C sort -u --parallel=8 \
	> $2 \
	|| (echo -e "$CLR_ERROR failed at convert_ttl_json" && exit 1) 
}

[ ! -d "./rdf_dumps" ] \
	&& echo -e "$CLR_INFO process download files" \
	&& wget -q --show-progress -i "./downloads" -P "./rdf_dumps" \
	|| echo -e "$CLR_WARN skipped download"

[ ! -f "./ppp.nt.left" ] \
	&& echo -e "$CLR_INFO process filter types for ppp iris and sort" \
	&& pre_ppp_iris "ppp.nt.left" \
	|| echo -e "$CLR_WARN skipped filter types for ppp iris and sort" 
 
[ ! -f "labels.nt" ] \
	&& echo -e "$CLR_INFO process sort labels" \
	&& pre_labels "labels.nt" \
	|| echo -e "$CLR_WARN skipped sort labels" \

[ ! -f "ppp_labels.nt" ] \
	&& echo -e "$CLR_INFO process join ppp entities with their labels" \
	&& join_labels_iris "ppp.nt.left" "labels.nt" "ppp_labels.nt" \
	|| echo -e "$CLR_WARN skipped join ppp entities with their labels"

[ ! -f "./PPP.json" ] \
	&& echo -e "$CLR_INFO process conversion ttl to json" \
	&& convert_ttl_json "ppp_labels.nt" "PPP.json" \
	|| echo -e "$CLR_WARN skipped conversion ttl to json"

