#!/usr/bin/env bash

index="$1"
[ -z "$index" ] && echo -n "index name: " && read index

file="$2"
[ -z "$file" ] && echo -n "file to load: " && read file

bulk="$file.bulk"

# create index
echo -e "\e[32;1m[INFO]\e[m create schema for ${index}"
code=$(curl -X PUT "localhost:9200/${index}?pretty" \
	-H 'Content-Type: application/json' \
	-w '%{http_code}' \
	-o /dev/null \
	-s -d'
{
  "settings": {
    "analysis": {
      "filter": {
        "ngram_filter": {
          "type": "edge_ngram",
          "min_gram": 3,
          "max_gram": 40
        }
      },
      "analyzer": {
        "ngram_analyzer": {
          "type": "custom",
          "tokenizer": "whitespace",
          "filter": [
            "lowercase",
            "ngram_filter"
          ]
        }
      }
    }
  },
  "mappings": {
    "properties": {
      "label": {
        "type": "text",
        "analyzer": "ngram_analyzer",
	"fields": {
	  "length": { 
	    "type": "token_count",
	    "analyzer": "standard"
  	  }
        }
      },
      "id": {
        "type": "text"
      }
    }
  }
}')

# response logging schema
[ "$code" -ne 200 ] \
	&& echo -e "\e[31;1m[ERROR]\e[m failed to create schema for ${index} ${code}" \
	|| echo -e "\e[32;1m[INFO]\e[m created schema for ${index} ${code} OK"  

# prepare for loading
sed -e 's|^|{"index":\{"_index"\:"'$index'","_type":"_doc"}}\n|g' $file \
	> $bulk

# load data
echo -e "\e[32;1m[INFO]\e[m load ${bulk} for ${index}"
code=$(curl -X POST "localhost:9200/_bulk" \
	-H 'Content-Type: application/x-ndjson' \
	-w '%{http_code}' \
	-o /dev/null \
	--data-binary @"$bulk")
	
# response logging data
[ "$code" -ne 200 ] \
	&& echo -e "\e[31;1m[ERROR]\e[m failed to load ${bulk} for ${index} ${code}" \
	|| echo -e "\e[32;1m[INFO]\e[m loaded ${bulk} for ${index} ${code} OK"  

# remove tmp data
rm $bulk
