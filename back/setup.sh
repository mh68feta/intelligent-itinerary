#!/usr/bin/env bash

docker run -d \
	--name i2-elasticsearch \
	-p 9200:9200 \
	-e "discovery.type=single-node" \
	elasticsearch:6.5.0

echo -e "\e[32;1m[INFO]\e[m wait for startup"
while [ "$(curl -s -o /dev/null -w '%{http_code}' 'localhost:9200')" -ne "200" ]
do
	sleep 1 && echo -n "."
done
echo -e "\e[32;1mDONE\e[m"

$(dirname $0)/load_suggest.sh ppp_suggest $(dirname $0)/dumps/PPP.json
$(dirname $0)/load_suggest.sh poi_suggest $(dirname $0)/dumps/POI.json

$(dirname $0)/suggest Leipz ppp_suggest
docker stop i2-elasticsearch && docker rm i2-elasticsearch

