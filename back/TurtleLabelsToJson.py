#!/usr/bin/env python3

#"ppp_labels.ttl"

import rdflib
import json
import sys

def parseRdf(path):
    arr=[]
    g = rdflib.Graph()
    g.parse(path, format="ttl")
    qres = g.query(
        """
        SELECT ?iri ?label
        {
            ?iri rdfs:label ?label .
        }
        """)
    for row in qres:
        iri=str(row['iri'])
        label=str(row['label'])
        arr.append({ 'iri': iri, 'label': label })

    return arr

ttlDict = parseRdf(sys.argv[1])

for x in ttlDict:
    print(json.dumps(x))
