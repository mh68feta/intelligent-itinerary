#!/usr/bin/env python3

import sys
import time
import json
from SPARQLWrapper import SPARQLWrapper, JSON

ReqSize = 5000
sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setReturnFormat(JSON)

prefixes = """
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
"""

def query(offset, limit):
     return prefixes + """
     SELECT DISTINCT ?type ?label {
        ?sub rdf:subClassOf* dbo:Place .
        ?ent a ?sub .
        ?ent dbo:type ?type .
        ?type rdfs:label ?label .
        FILTER( lang(?label) = 'en' )
     } OFFSET %s LIMIT %s
     """ % (offset, limit)


def get_POIType_list():
    lPOITypes = []
    i = 0
    while True:
        size = len(lPOITypes)
        sparql.setQuery(query(i*ReqSize, ReqSize))
        lPOITypes += sparql.query().convert()["results"]["bindings"]
        i += 1
        if(len(lPOITypes) == size):
            break

    return lPOITypes

if __name__ == "__main__":
    with open('./dumps/POI.json', 'a', encoding='utf-8') as outfile:
        c = 0
        for item in get_POIType_list():
            iri = item['type']['value']
            label = item['label']['value']
            response = {}
            response.update({'iri': iri, 'label': label})

            outfile.write(json.dumps(response) + '\n')
            c += 1
            if(c % 50 == 0):
                print("             \r{}".format(c), end=' ')
                sys.stdout.flush()
        print("             \r{}".format(c))
