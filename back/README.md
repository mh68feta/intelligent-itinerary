# Backend

# Generate Data

Setup
```
pip3 install -r ./requirements.txt
```

For PPPs
```
bash ./fetchPPPs.py
```

For PPPTypes
```
pyhton3 ./fetchPOITypes.py
```

# Testing

Test full setup: docker run, loading, suggest query
```
bash ./setup.sh
```

Single suggest test
```
# ./suggest.sh $index $term
./suggest.sh ppp_suggest Leipz
```
