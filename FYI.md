## FYI

Usefull stuff

### Git-LFS

> https://git-lfs.github.com

Debian/Ubuntu

```bash
apt-get install git-lfs
```

Usage

```bash
# setup
cd $REPOSITORY
git lfs install
git add .gitattributes

# add (e.g. bz2 files)
git lfs track "*.bz2"
git add '*.bz2'
```

### Lbzip2

Parallel version of the *bzip2* compression, that can also decompress concatenated bzip2 files (e.g. by using cat to concat).

```bash
apt-get install lbzip2
```

Usage

```bash
# compress
lbzip2 <file>
# uncompress
lbunzip2 <file>
# cat
lbzcat <file>
```
