from datetime import datetime, timedelta
import requests

# api key for darksky weather
DARK_SKY_API_KEY = '4ff4ee272338a2801bf5071bd78e06d8'

def get_current_weather(lat, lon):
    search_date = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
    r = requests.get(
        "https://api.darksky.net/forecast/" + DARK_SKY_API_KEY + "/" + lat + "," + lon + "," + search_date,
        "units=si").json()
    # print(json.dumps(r, indent=4, sort_keys=True))
    rc = r["currently"]
    json_response = {}
    tzone = r["timezone"]
    time = convert_seconds(rc["time"])
    temp = rc["temperature"]
    atemp = rc["apparentTemperature"]
    wind = rc["windSpeed"]
    summ = rc["summary"]
    icon = rc["icon"]
    pcpr = rc["precipProbability"]
    pcin = rc["precipIntensity"]
    json_response.update(
        {"timezone": tzone, "time": time, "temperature": temp, "apparentTemperature": atemp, "windSpeed": wind,
         "summary": summ, "icon": icon, "precipProbability": pcpr, "precipIntensity": pcin})
    return json_response

def get_daily_weather(lat, lon):
    search_date = datetime.now().strftime('%Y-%m-%dT%H:%M:%S')
    r = requests.get(
        "https://api.darksky.net/forecast/" + DARK_SKY_API_KEY + "/" + lat + "," + lon + "," + search_date,
        "units=si").json()
    rd = r["daily"]["data"][0]
    json_response = {}
    tzone = r["timezone"]
    time = convert_date(rd["time"])
    thid = rd["temperatureHigh"]
    thit = convert_time(rd["temperatureHighTime"])
    tlod = rd["temperatureLow"]
    tlot = convert_time(rd["temperatureLowTime"])
    wind = rd["windSpeed"]
    summ = rd["summary"]
    icon = rd["icon"]
    pcpr = rd["precipProbability"]
    pcin = rd["precipIntensity"]
    pcty = rd["precipType"]
    sunr = convert_time(rd["sunriseTime"])
    suns = convert_time(rd["sunsetTime"])
    json_response.update({"timezone": tzone, "date": time, "temperatureHigh": thid, "temperatureHighTime": thit,
                          "temperatureLow": tlod, "temperatureLowTime": tlot, "windSpeed": wind, "summary": summ,
                          "icon": icon, "precipProbability": pcpr, "precipIntensity": pcin, "precipType": pcty,
                          "sunriseTime": sunr, "sunsetTime": suns})
    return json_response

def get_weather_forecast_7days(lat, lon):
    json_response = {'day': []}
    today = datetime.now().strftime("%d.%m.%Y")
    date_from = datetime.strptime(today, "%d.%m.%Y")
    for x in range(7):
        new_date = (date_from + timedelta(days=x)).strftime("%Y-%m-%d")
        search_date = new_date + "T00:00:00"
        r = requests.get(
        "https://api.darksky.net/forecast/" + DARK_SKY_API_KEY + "/" + lat + "," + lon + "," + search_date,
        "units=si").json()
        rd = r["daily"]["data"][0]
        tzone = r["timezone"]
        time = convert_date(rd["time"])
        thid = rd["temperatureHigh"]
        thit = convert_time(rd["temperatureHighTime"])
        tlod = rd["temperatureLow"]
        tlot = convert_time(rd["temperatureLowTime"])
        wind = rd["windSpeed"]
        icon = rd["icon"]
        pcpr = rd["precipProbability"]
        pcin = rd["precipIntensity"]
        pcty = rd["precipType"]
        sunr = convert_time(rd["sunriseTime"])
        suns = convert_time(rd["sunsetTime"])
        json_response['day'].append(
            {"timezone": tzone, "date": time, "temperatureHigh": thid, "temperatureHighTime": thit,
                     "temperatureLow": tlod, "temperatureLowTime": tlot, "windSpeed": wind, "icon": icon,
                     "precipProbability": pcpr, "precipIntensity": pcin, "precipType": pcty,
                     "sunriseTime": sunr,
                     "sunsetTime": suns})
    return json_response

def get_weather_forecast(lat, lon, df, dt):
    json_response = {'day': []}
    date_from = datetime.strptime(df, "%d.%m.%Y")
    date_to = datetime.strptime(dt, "%d.%m.%Y")
    delta = date_to - date_from
    for x in range(delta.days + 1):
        new_date = (date_from + timedelta(days=x)).strftime("%Y-%m-%d")
        search_date = new_date + "T00:00:00"
        r = requests.get(
        "https://api.darksky.net/forecast/" + DARK_SKY_API_KEY + "/" + lat + "," + lon + "," + search_date,
        "units=si").json()
        rd = r["daily"]["data"][0]
        tzone = r["timezone"]
        time = convert_date(rd["time"])
        thid = rd["temperatureHigh"]
        thit = convert_time(rd["temperatureHighTime"])
        tlod = rd["temperatureLow"]
        tlot = convert_time(rd["temperatureLowTime"])
        wind = rd["windSpeed"]
        sunr = convert_time(rd["sunriseTime"])
        suns = convert_time(rd["sunsetTime"])
        json_response['day'].append(
            {"timezone": tzone, "date": time, "temperatureHigh": thid, "temperatureHighTime": thit,
                     "temperatureLow": tlod, "temperatureLowTime": tlot, "windSpeed": wind, "sunriseTime": sunr,
                     "sunsetTime": suns})
    return json_response

def convert_seconds(s):
    return datetime.fromtimestamp(s).strftime("%d.%m.%Y:%H:%M")

def convert_date(s):
    return datetime.fromtimestamp(s).strftime("%d.%m.%Y")

def convert_time(s):
    return datetime.fromtimestamp(s).strftime("%H:%M")
