#!/usr/bin/env python3

from flask import Flask, jsonify, request, Response
from flask_swagger import swagger
from datetime import datetime
from suggest import suggest_poi, suggest_ppp
from dbpedia_sparql import get_poi_in_range, get_ppp_in_range, get_ppp_label_info, get_ppp_iri_info, get_poi_label_info
from weather import get_current_weather, get_daily_weather, get_weather_forecast_7days, get_weather_forecast
from journey import get_station, get_journey
import json
from flask_cors import CORS

IP='0.0.0.0'
PORT=5000

app = Flask(__name__)
CORS(app)

@app.route("/")
def root():
    html = """
    <!DOCTYPE html>
    <html>
    <head>
    <title>ReDoc</title>
    <!-- needed for adaptive design -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:300,400,700" rel="stylesheet">
    <!--
    ReDoc doesn't change outer page styles
    -->
    <style>
      body {
        margin: 0;
        padding: 0;
      }
    </style>
    </head>
    <body>
    <redoc spec-url='/spec'></redoc>
    <script src="https://cdn.jsdelivr.net/npm/redoc@next/bundles/redoc.standalone.js"> </script>
    </body>
    </html>
    """
    return html

@app.route("/spec")
def spec():                 
    swag = swagger(app)
    swag['info']['version'] = "1.0"
    swag['info']['title'] = "Intelligent Itinerary"
    return jsonify(swag) 

@app.route("/weather/currently", methods=['GET'])
def weather_currently():
    """
    Get current weather
    ---
    tags:
      - weather
    definitions:
      - schema:
          id: Weather
          properties:
            timezone:
              type: string
              description: timezone
            time:
              type: string
              format: date
              description: dd.mm.YYYY:HH:MM
            summary:
              type: string
              description: summary of the current weather
            icon:
              type: string
              description: id of the icon
            temperature:
              type: number
              format: float
            apparentTemperature:
              type: number
              format: float
            dewPoint:
              type: number
              format: float
            humidity:
              type: number
              format: float
            pressure:
              type: number
              format: float
            windSpeed:
              type: number
              format: float
    parameters:
      - in: "query"
        name: "lat"
        description: "latitude"
        required: true
        type: "number"
        format: "float"
      - in: "query"
        name: "long"
        description: "longitude"
        required: true
        type: "number"
        format: "float"
    responses:
      200:
        content:
          application/json:
            schema:
              $ref: "#/definitions/Weather"
        description: currently weather response
    """
    response = get_current_weather(request.args["lat"],request.args["long"])
    return Response(json.dumps(response), mimetype='application/json')

@app.route("/weather/daily", methods=['GET'])
def weather_daily():
    """
    Get daily weather
    ---
    tags:
      - weather
    parameters:
      - in: "query"
        name: "lat"
        description: "latitude"
        required: true
        type: "number"
        format: "float"
      - in: "query"
        name: "long"
        description: "longitude"
        required: true
        type: "number"
        format: "float"
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                  timezone:
                    type: string
                    description: timezone
                  date:
                    type: date
                    description: dd.mm.YYYY
                  temperatureHigh:
                    type: number
                    format: float
                  temperatureHighTime:
                    type: time
                    format: "time"
                  temperaturLow:
                    type: number
                    format: "float"
                  temperatureLowTime:
                    type: time
                    description: HH:MM
                  windSpeed:
                    type: number
                    format: float
                    description: wind speed in m/s
                  summary:
                    type: string
                    description: summary of the weather
                  icon:
                    type: string
                    description: id of the animated icon
                  precipProbability:
                    type: number
                    format: float
                  precipIntensity:
                    type: number
                    format: float
                  precipType:
                    type: string
                    format: string
                  sunriseTime:
                    type: time
                    format: time
                  sunsetTime:
                    type: time
                    format: time
        description: daily weather response
    """
    response = get_daily_weather(request.args["lat"], request.args["long"])
    return Response(json.dumps(response), mimetype='application/json')

@app.route("/weather/forecast/7days", methods=['GET'])
def weather_forecast_7days():
    """
    Get weather forecast 7 days
    ---
    tags:
      - weather
    parameters:
      - in: "query"
        name: "lat"
        description: "latitude"
        required: true
        type: "number"
        format: "float"
      - in: "query"
        name: "long"
        description: "longitude"
        required: true
        type: "number"
        format: "float"
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                  timezone:
                    type: string
                    description: timezone
                  day:
                    type: array
                    items:
                      type: object
                      properties:
                        date:
                          type: date
                          description: dd.mm.YYYY
                        temperatureHigh:
                          type: number
                          format: float
                        temperatureHighTime:
                          type: time
                          format: "time"
                        temperaturLow:
                          type: number
                          format: "float"
                        temperatureLowTime:
                          type: time
                          description: HH:MM
                        windSpeed:
                          type: number
                          format: float
                          description: wind speed in m/s
                        icon:
                          type: string
                          description: id of the animated icon
                        precipProbability:
                          type: number
                          format: float
                        precipIntensity:
                          type: number
                          format: float
                        precipType:
                          type: string
                          format: string
                        sunriseTime:
                          type: time
                          format: time
                        sunsetTime:
                          type: time
                          format: time
        description: weather forecast next 7 days response
    """
    response = get_weather_forecast_7days(request.args["lat"], request.args["long"])
    return Response(json.dumps(response), mimetype='application/json')

@app.route("/weather/forecast", methods=['GET'])
def weather_forecast():
    """
    Get weather forecast
    ---
    tags:
      - weather
    parameters:
      - in: "query"
        name: "lat"
        description: "latitude"
        required: true
        type: "number"
        format: "float"
      - in: "query"
        name: "long"
        description: "longitude"
        required: true
        type: "number"
        format: "float"
      - in: "query"
        name: "df"
        description: "date_from"
        required: true
        type: "date"
        format: "string"
      - in: "query"
        name: "dt"
        description: "date_to"
        required: true
        type: "date"
        format: "string"
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                  latitude:
                    type: number
                    format: float
                    description: latitude
                  longitude:
                    type: number
                    format: float
                    description: longitude
                  timezone:
                    type: string
                    description: timezone
                  day:
                    type: array
                    items:
                      type: object
                      properties:
                        date:
                          type: date
                          description: dd.mm.YYYY
                        temperatureHigh:
                          type: number
                          format: float
                        temperatureHighTime:
                          type: time
                          format: "time"
                        temperaturLow:
                          type: number
                          format: "float"
                        temperatureLowTime:
                          type: time
                          description: HH:MM
                        windSpeed:
                          type: number
                          format: float
                          description: wind speed in m/s
                        sunriseTime:
                          type: time
                          format: time
                        sunsetTime:
                          type: time
                          format: time
        description: weather forecast response
    """
    response = get_weather_forecast(request.args["lat"], request.args["long"], request.args["df"], request.args["dt"])
    return Response(json.dumps(response), mimetype='application/json')

@app.route("/station", methods=['GET'])
def station():
    """
    Get station by name
    ---
    tags:
      - journey
    parameters:
      - in: "query"
        name: "name"
        description: "name of the location"
        required: true
        type: "string"
        format: "string"
    responses:
      200:
        content:
          application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
                  format: string
                  description: name of the station
                longitude:
                  type: number
                  format: float
                  description: longitude
                latitude:
                  type: number
                  format: float
                  description: latitude
                id:
                  type: integer
                  description: id of the station
        description: db-rest station api response
    """
    response = get_station(request.args["name"])
    return Response(json.dumps(response), mimetype='application/json')

@app.route("/journey", methods=['GET'])
def journey():
    """
    Get journey for two given points
    ---
    tags:
      - journey
    parameters:
      - in: "query"
        name: "from"
        description: "name of the location"
        required: true
        type: "string"
        format: "string"
      - in: "query"
        name: "to"
        description: "name of the location"
        required: true
        type: "string"
        format: "string"
      - in: "query"
        name: "date"
        description: "yyyy-mm-ddTHH:MM"
        required: false
        type: "date"
        format: "string"
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                  price:
                    type: float
                    format: number
                    description: price in €
                  departure:
                    type: array
                    items:
                      type: object
                      properties:
                        scheduled:
                          type: object
                          format: timestamp
                          description: YYYY-MM-dd:HH:mm:sss
                        platform:
                          type: object
                          format: string
                          description: platform where the train arrives
                        direction:
                          type: object
                          format: string
                          description: where the train is heading to
                        line:
                          type: object
                          format: string
                          description: name and number of the train
                  arrival:
                    type: array
                    items:
                      type: object
                      properties:
                        scheduled:
                          type: object
                          format: timestamp
                          description: YYYY-MM-dd:HH:mm:sss
                        platform:
                          type: object
                          format: string
                          description: platform where the train departs
                        origin:
                          type: object
                          format: string
                          description: where the train comes from
                        destination:
                          type: object
                          format: string
                          description: destination of the journey
        description: db-rest journey api response
    """
    if 'date' in request.args:
        search_date = request.args["date"]
    else:
        search_date = datetime.now().strftime('%Y-%m-%dT%H:%M')
    response = get_journey(request.args["from"], request.args["to"], search_date)
    return Response(json.dumps(response), mimetype='application/json')

@app.route("/ppp/suggest/<term>", methods=['GET'])
def ppp_suggest(term):
    """
    Suggest/Autocomplete populated places
    ---
    tags:
      - ppp
    parameters:
      - in: "path"
        name: "term"
        description: "poi search as you type term"
        required: true
        type: string
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                  label:
                    type: string
                  dboType:
                    type: string
        description: ppp response
    """
    response = suggest_ppp(term)
    return Response(json.dumps(response),mimetype='application/json')

@app.route("/poi/suggest/<term>", methods=['GET'])
def poi_suggest(term):
    """
    Suggest/Autocomplete point of interest
    ---
    tags:
      - poi
    parameters:
      - in: "path"
        name: "term"
        description: "poi search as you type term"
        required: true
        type: string
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                  label:
                    type: string
                  dboType:
                    type: string
        description: poi response
    """
    response = suggest_poi(term)
    return Response(json.dumps(response),mimetype='application/json')
    
@app.route("/poi/range", methods=['GET'])
def poi_range():
    """
    Get point of interest in range
    ---
    tags:
      - poi
    parameters:
      - in: "query"
        name: "dboType"
        description: "poi label"
        required: true
        type: string
        description: "dbpedia ontology type"
      - in: "query"
        name: "lat"
        required: true
        type: string
        description: "latitude"
      - in: "query"
        name: "long"
        required: true
        type: string
        description: "longitude"
      - in: "query"
        name: "radius"
        required: true
        type: string
        description: "radius"
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                  iri:
                    type: string
                  label:
                    type: string
                  comment:
                    type: string
                  lat:
                    type: string
                  long:
                    type: string
        description: poi response
    """
    response = get_poi_in_range(request.args["dboType"],request.args["lat"],request.args["long"],request.args["radius"])
    return Response(json.dumps(response),mimetype='application/json')

@app.route("/ppp/range", methods=['GET'])
def ppp_range():
    """
    Get populated places in range
    ---
    tags:
      - ppp
    parameters:
      - in: "query"
        name: "lat"
        required: true
        type: string
        description: "latitude"
      - in: "query"
        name: "long"
        required: true
        type: string
        description: "longitude"
      - in: "query"
        name: "radius"
        required: true
        type: string
        description: "radius"
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                  iri:
                    type: string
                  label:
                    type: string
                  lat:
                    type: string
                  long:
                    type: string
        description: poi response
    """
    response = get_ppp_in_range(request.args["long"],request.args["lat"],request.args["radius"])
    return Response(json.dumps(response),mimetype='application/json')
@app.route("/poi/info/<label>", methods=['GET'])
def poi_label_info(label):
    """
    Info point of interest for given label
    ---
    tags:
      - poi
    parameters:
      - in: "path"
        name: "label"
        description: "ppp label"
        required: true
        type: string
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                  iri:
                    type: string
                  label:
                    type: string
                  comment:
                    type: string
                  lat:
                    type: string
                  long:
                    type: string
                  thumbnail: 
                    type: string
        description: ppp response
    """
    response = get_poi_label_info(label)
    return Response(json.dumps(response),mimetype='application/json')

@app.route("/ppp/info/<label>", methods=['GET'])
def ppp_label_info(label):
    """
    Info populated place for given label
    ---
    tags:
      - ppp
    parameters:
      - in: "path"
        name: "label"
        description: "poi label"
        required: true
        type: string
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                  iri:
                    type: string
                  label:
                    type: string
                  comment:
                    type: string
                  lat:
                    type: string
                  long:
                    type: string
                  thumbnail: 
                    type: string
        description: ppp response
    """
    response = get_ppp_label_info(label)
    return Response(json.dumps(response),mimetype='application/json')

@app.route("/ppp/info", methods=['GET'])
def ppp_iri_info():
    """
    Info populated place for given iri
    ---
    tags:
      - ppp
    parameters:
      - in: "query"
        name: "iri"
        description: "ppp iri"
        required: true
        type: string
    responses:
      200:
        content:
          application/json:
            schema:
              type: array
              items:
                type: object
                properties:
                properties:
                  iri:
                    type: string
                  label:
                    type: string
                  comment:
                    type: string
                  lat:
                    type: string
                  long:
                    type: string
                  populationTotal:
                    type: string
                  populationDensity:
                    type: string
                  areaMetro:
                    type: string
                  thumbnail: 
                    type: string
                  areaTotal:
                    type: string
        description: ppp response
    """
    if "iri" in request.args:
      response = get_ppp_iri_info(request.args["iri"])
      return Response(json.dumps(response),mimetype='application/json')
    else:
      return Response("{}",mimetype='application/json')
    
if __name__ == "__main__":
    app.run(host=IP, port=PORT)
