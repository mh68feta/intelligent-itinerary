
import time
import json
from queries import sparql_type_in_range, sparql_ppp_in_range, sparql_ppp_iri_info, sparql_ppp_label_info, sparql_poi_label_info
from SPARQLWrapper import SPARQLWrapper,JSON

def get_poi_in_range(dbo_type,lat,lon,radius):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setReturnFormat(JSON)

    query_result_list = []
    sparql.setQuery(sparql_type_in_range(dbo_type,lat,lon,radius))
    query_result_list += sparql.query().convert()["results"]["bindings"]
    
    tmp_res = {}
    json_response = []
    for row in query_result_list:
        iri = str(row["place"]["value"])
        label = str(row["label"]["value"])
        comment = str(row["comment"]["value"])
        lat = str(row["lat"]["value"])
        lon = str(row["long"]["value"])
        tmp_res.update({ iri : { "label": label, "comment": comment, "lat": lat, "long": lon }})
    for k in tmp_res.keys():
        json_response.append({ "iri": k, "label": tmp_res[k]['label'], "comment": tmp_res[k]['comment'], "lat": tmp_res[k]['lat'], "long": tmp_res[k]['long'] })
    return json_response

def get_ppp_in_range(lat,lon,radius):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setReturnFormat(JSON)

    query_result_list = []
    sparql.setQuery(sparql_ppp_in_range(lat,lon,radius))
    query_result_list += sparql.query().convert()["results"]["bindings"]
    
    json_response = []
    for row in query_result_list:
        iri = str(row["place"]["value"])
        label = str(row["label"]["value"])
        lat = str(row["lat"]["value"])
        lon = str(row["long"]["value"])
        json_response.append({ "iri": iri, "label": label, "lat": lat, "long": lon })
    return json_response

#print(json.dumps(get_typed_in_range("http://dbpedia.org/resource/Art_museum","13.383333","52.516666","100")))

def get_ppp_iri_info(iri):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setReturnFormat(JSON)

    query_result_list = []
    sparql.setQuery(sparql_ppp_iri_info(iri))
    query_result_list += sparql.query().convert()["results"]["bindings"]
    
    json_response = {}
    for row in query_result_list:
        label = str(row["label"]["value"])
        comment = str(row["comment"]["value"])
        lat = str(row["lat"]["value"])
        lon = str(row["long"]["value"])
        areaTotal = ""
        if "areaTotal" in row:
            areaTotal = str(row["areaTotal"]["value"])
        areaMetro = ""
        if "areaMetro" in row:
            areaMetro = str(row["areaMetro"]["value"])
        populationTotal = ""
        if "populationTotal" in row:
            populationTotal = str(row["populationTotal"]["value"])
        populationDensity = ""
        if "populationDensity" in row:
            populationDensity = str(row["populationDensity"]["value"])
        thumbnail = ""
        if "thumbnail" in row:
            thumbnail = str(row["thumbnail"]["value"])
        json_response.update({ "iri": iri, "label": label, "comment": comment, "lat": lat, "long": lon, "populationTotal": populationTotal, "populationDensity": populationDensity, "areaMetro": areaMetro, "thumbnail": thumbnail, "areaTotal": areaTotal })
    return json_response

#print(json.dumps(gep_ppp_info("http://dbpedia.org/resource/Berlin")))

def get_poi_label_info(label):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setReturnFormat(JSON)

    query_result_list = []
    sparql.setQuery(sparql_poi_label_info(label))
    query_result_list += sparql.query().convert()["results"]["bindings"]

    json_response = {}
    for row in query_result_list:
        iri = str(row["iri"]["value"])
        comment = str(row["comment"]["value"])
        lat = str(row["lat"]["value"])
        lon = str(row["long"]["value"])
        thumbnail = ""
        if "thumbnail" in row:
            thumbnail = str(row["thumbnail"]["value"])
        json_response.update({ "iri": iri, "label": label, "comment": comment, "lat": lat, "long": lon, "thumbnail": thumbnail})
    return json_response

def get_ppp_label_info(label):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    sparql.setReturnFormat(JSON)

    query_result_list = []
    sparql.setQuery(sparql_ppp_label_info(label))
    query_result_list += sparql.query().convert()["results"]["bindings"]
    
    json_response = {}
    for row in query_result_list:
        iri = str(row["iri"]["value"])
        comment = str(row["comment"]["value"])
        lat = str(row["lat"]["value"])
        lon = str(row["long"]["value"])
        areaTotal = ""
        if "areaTotal" in row:
            areaTotal = str(row["areaTotal"]["value"])
        areaMetro = ""
        if "areaMetro" in row:
            areaMetro = str(row["areaMetro"]["value"])
        populationTotal = ""
        if "populationTotal" in row:
            populationTotal = str(row["populationTotal"]["value"])
        populationDensity = ""
        if "populationDensity" in row:
            populationDensity = str(row["populationDensity"]["value"])
        thumbnail = ""
        if "thumbnail" in row:
            thumbnail = str(row["thumbnail"]["value"])
        json_response.update({ "iri": iri, "label": label, "comment": comment, "lat": lat, "long": lon, "populationTotal": populationTotal, "populationDensity": populationDensity, "areaMetro": areaMetro, "thumbnail": thumbnail, "areaTotal": areaTotal })
    return json_response
