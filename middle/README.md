# Middleware

## Setup

Install python and pip

```bash
# DEBIAN/UBUNTU
sudo apt-get install python3 python3-pip
```

Install python virtual environment

```bash
pip3 install virtualenv
```

Setup virtual environment
```bash
virtualenv -p python3.6 venv
# or try
python3 -m virtualenv -p python3.6 venv
```

Enter virtual  environemnt 
```bash
source venv/bin/activate
```

Install dependecies
```bash
pip3 install -r requirements.txt
```

Run ReST API
```bash
python rest.py
```

## Example cURL

`/poi/suggest/<term>`
```
curl -X GET "http://localhost:5000/poi/suggest/Art"
```

`/poi/range`
```
curl -X GET "http://localhost:5000/poi/range?dboType=http%3A%2F%2Fdbpedia.org%2Fresource%2FArt_museum&long=13.383333&lat=52.516666&radius=100"
```

`/ppp/range`
```
curl -X GET "http://localhost:5000/ppp/range?lat=51.0&long=9.0&radius=100"

```
`/ppp/suggest/<term>`
```
# TODO no implemented yet
curl -X GET "http://localhost:5000/ppp/suggest/Art"
```

`/ppp/info/<label>`
```
curl -X GET "http://localhost:5000/ppp/info/New%20York%20City"
```

`/ppp/info`
```
curl -X GET "http://localhost:5000/ppp/info?iri=http%3A%2F%2Fdbpedia.org%2Fresource%2FBerlin"
```

`/weather/currently`
```
curl -X GET "http://localhost:5000/weather/currently?lat=52.520008&long=13.404954"
```

`/weather/daily`
```
curl -X GET "http://localhost:5000/weather/daily?lat=52.520008&long=13.404954"
```

`/weather/forecast/7days`
```
curl -X GET "http://localhost:5000/weather/forecast/7days?lat=52.520008&long=13.404954"
```

`/weather/forecast`
```
curl -X GET "http://localhost:5000/weather/forecast?lat=52.520008&long=13.404954&df=01.01.2020&dt=08.01.2020"
```

`/journey`
```
curl -X GET "http://localhost:5000/journey?from=Berlin&to=Leipzig&2020-01-26T15:30"
```

## Queries

* [OSM sparql endpoint](https://sophox.org/) with federeated queries enabled

```sparql
SELECT * WHERE {

  ?id osmt:name:en "Berlin" ;
      osmt:place "city" ;
      osmt:wikidata ?wd .
  
  SERVICE  <https://query.wikidata.org/sparql> {
    ?wd ?p ?o .  
  }
}
```

## Weather Units

* summary: Any summaries containing temperature or snow accumulation units will have their values in degrees Celsius or in centimeters (respectively).
* nearestStormDistance: Kilometers.
* precipIntensity: Millimeters per hour.
* precipIntensityMax: Millimeters per hour.
* precipAccumulation: Centimeters.
* temperature: Degrees Celsius.
* temperatureMin: Degrees Celsius.
* temperatureMax: Degrees Celsius.
* apparentTemperature: Degrees Celsius.
* dewPoint: Degrees Celsius.
* windSpeed: Meters per second.
* windGust: Meters per second.
* pressure: Hectopascals.
* visibility: Kilometers.
