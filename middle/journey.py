from datetime import datetime, timedelta
import requests
import ndjson
import json

# api key for DB
DB_BEARER = '2abd33aba017591e47a66d213b7016d2'

# https://2.db.transport.rest/journeys?from=8010136&to=8010205&results=1&departure=2020-01-30T14:00

class BearerAuth(requests.auth.AuthBase):
    def __init__(self, token):
        self.token = token

    def __call__(self, r):
        r.headers["authorization"] = "Bearer " + self.token
        return r

def get_station(name):
    return requests.get("https://api.deutschebahn.com/fahrplan-plus/v1/location/"
                        + name, auth=BearerAuth(DB_BEARER)).json()[0]


def get_id(name):
    return str(requests.get("https://api.deutschebahn.com/fahrplan-plus/v1/location/"
                            + name, auth=BearerAuth(DB_BEARER)).json()[0]["id"])


def get_id_hafas(name):
    stationLine = None
    r = requests.get("https://2.db.transport.rest/stations?name=" + name)
    items = r.json(cls=ndjson.Decoder)

    count = 1
    while stationLine is None:
        for i in items:
            if i["category"] == count:
                stationLine = i
                break
        count += 1

    return stationLine["id"]


def get_journey(fr, to, date):
    results = str(5)
    new_date = str(datetime.strptime(date, '%Y-%m-%dT%H:%M') - timedelta(hours=1))
    r = requests.get("https://2.db.transport.rest/journeys?from=" + get_id_hafas(fr) + "&to=" + get_id_hafas(to) +
                     "&departure=" + new_date + "&results=" + results).json()
    json_response = {'journey': []}

    # print("from: " + fr + " to: " + to)
    # print("from: " + get_id_hafas(fr) + " to: " + get_id_hafas(to))
    # print(json.dumps(r, indent=4))

    journey = 0
    for i in r:
        price = i["price"]["amount"]
        json_response['journey'].append({"price": price, 'stops': []})
        for x in i["legs"]:
            if "departurePlatform" in x:
                # departure
                dep_platform = x["departurePlatform"]
                departure = x["departure"]
                direction = x["direction"]
                line = x["line"]["name"]
                # arrival
                arrival = x["arrival"]
                arr_platform = x["arrivalPlatform"]
                destination = x["destination"]["name"]
                origin = x["origin"]["name"]
                json_response['journey'][journey]['stops'].append({
                    "departure": {"scheduled": departure, "platform": dep_platform, "direction": direction,
                                  "line": line},
                    "arrival": {"scheduled": arrival, "platform": arr_platform, "origin": origin,
                                "destination": destination}})
        journey += 1
    return json_response
