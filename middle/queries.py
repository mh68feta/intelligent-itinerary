prefixes = """
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
"""

def sparql_type_in_range(dbo_type, lat, lon, radius):
    return prefixes + """
    SELECT DISTINCT ?place, ?label, ?lat, ?long, ?comment { 
        ?place dbo:type <%s> ;
            rdfs:label ?label ;
            rdfs:comment ?comment ;
            geo:lat ?lat ;
            geo:long ?long .
        FILTER ( bif:st_intersects( bif:st_point (?long, ?lat), bif:st_point ( %s, %s), %s))
        FILTER ( lang(?comment) = 'en' )
        FILTER ( lang(?label) = 'en' )
    } Limit 100
    """ % (dbo_type,lon,lat,radius)

def sparql_ppp_in_range(lat, lon, radius):
    return prefixes + """
    SELECT DISTINCT ?place, ?label, ?lat, ?long { 
         ?place a dbo:PopulatedPlace ;      
             rdfs:label ?label ;  
             # rdfs:comment ?comment ;    
             geo:lat ?lat ;
             geo:long ?long .
         FILTER ( bif:st_intersects( bif:st_point (?long, ?lat), bif:st_point ( %s, %s), %s))
         # FILTER ( lang(?comment) = 'en' )
         FILTER ( lang(?label) = 'en' )
     } Limit 100    
    """ % (lat,lon,radius)

def sparql_ppp_suggest(label):
    return prefixes + """
    SELECT * {
        ?s a dbo:PopulatedPlace .
        ?s rdfs:label ?name . 
        FILTER( lang(?name) = 'en' )
        ?name bif:contains "'%s*'".
    } Limit 10
    """ % label

def sparql_ppp_iri_info(iri):
    return prefixes + """
    PREFIX dbopp: <http://dbpedia.org/ontology/PopulatedPlace/>   
    SELECT * {
        VALUES ?place { <%s> }
        
        ?place rdfs:label ?label;
        rdfs:comment ?comment;
        geo:lat ?lat;
        geo:long ?long.
        
        FILTER( lang(?label) = 'en' )
        FILTER( lang(?comment) = 'en' )
        
        OPTIONAL { ?place dbopp:areaMetro ?areaMetro . }	
        OPTIONAL { ?place dbopp:areaTotal ?areaTotal . }
        OPTIONAL { ?place dbopp:populationDensity ?populationDensity . }
        OPTIONAL { ?place dbo:thumbnail ?thumbnail . }
        OPTIONAL { ?place dbo:populationTotal ?populationTotal . }

    } Limit 1
    """ % iri

def sparql_ppp_label_info(label):
    return prefixes + """
    PREFIX dbopp: <http://dbpedia.org/ontology/PopulatedPlace/>   
    SELECT * {
        
        ?iri rdfs:label "%s"@en;
        rdfs:comment ?comment;
        geo:lat ?lat;
        geo:long ?long.
        
        FILTER( lang(?comment) = 'en' )
        
        OPTIONAL { ?iri dbopp:areaMetro ?areaMetro . }	
        OPTIONAL { ?iri dbopp:areaTotal ?areaTotal . }
        OPTIONAL { ?iri dbo:populationDensity ?populationDensity . }
        OPTIONAL { ?iri dbo:thumbnail ?thumbnail . }
        OPTIONAL { ?iri dbo:populationTotal ?populationTotal . }

    } Limit 1
    """ % label
    
def sparql_poi_label_info(label):
    return prefixes + """
    SELECT * {  
        ?iri rdfs:label "%s"@en;
        rdfs:comment ?comment;
        geo:lat ?lat;
        geo:long ?long.
        FILTER( lang(?comment) = 'en' )
        OPTIONAL { ?iri dbo:thumbnail ?thumbnail }
    } Limit 1
    """ % label
