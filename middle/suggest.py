import json
import requests

#ip="v122.de:9200"
#ip="localhost:9200" 
ip="backend:9200"

def suggest_poi(label):
    index="poi_suggest"
    url = "http://%s/%s/_search" % (ip,index)
    payload = {
        "query": {
            "match": {
                "label": label
            }
        }
    }

    headers = {'content-type': 'application/json'}
    r = requests.get(url, data=json.dumps(payload), headers=headers).json()

    json_response = []
    for entry in r["hits"]["hits"]:
        json_response.append(entry["_source"])

    return json_response

def suggest_ppp(label):
    index="ppp_suggest"
    url = "http://%s/%s/_search" % (ip,index)
    payload = {
        "query": {
            "match": {
                "label": label
            }
        }
    }

    headers = {'content-type': 'application/json'}
    r = requests.get(url, data=json.dumps(payload), headers=headers).json()

    json_response = []
    for entry in r["hits"]["hits"]:
        json_response.append(entry["_source"])

    return json_response

