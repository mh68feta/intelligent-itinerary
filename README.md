# Intelligent Itinerary

## Setup

> REQUIREMENTS: [docker](https://docs.docker.com/install/), [docker-compose](https://docs.docker.com/compose/install/), 
curl, bash git, [git-lfs](https://git-lfs.github.com), yarn

## Automated Build

```
# ./setup --and-build to force docker-compose build
./setup.sh
```

This script tries an automated setup on your machine.
* Building the docker files
* Starting the docker containers
* Loading the backend data

## Manual Build

```
git clone https://git.informatik.uni-leipzig.de/mh68feta/intelligent-itinerary.git
cd intelligent-itinerary
git lfs pull
```

```
docker-compose up -d

```

Load backend
```
back/load_suggest.sh ppp_suggest back/dumps/PPP.json
back/load_suggest.sh poi_suggest back/dumps/POI.json
```

* Visit [localhost:3000](http://localhost:3000)

* ReDoc [localhost:5000](http://localhost:5000)

## Clean up

To stop and remove all associated docker containers
```
docker-compose down
```

## TODO

* [Issues](https://git.informatik.uni-leipzig.de/mh68feta/intelligent-itinerary/issues)

# MISC

Change host in frontend
```
sed -i 's|http://localhost:5000/|http://v122.de:5000/|g' $(find front/build/ -type f)
```
